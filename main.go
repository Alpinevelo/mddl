package main

import (
	"fmt"
	"os"
	"strconv"
	"strings"
)

var configDir string
var archiveDir string

func main() {
	setup()

	if len(os.Args) == 1 {
		printMenu()
		os.Exit(0)
	}

	switch os.Args[1] {
	case "setup":
		firstTimeSetup()
	case "fetch":
		fetch()
		break
	default:
		printMenu()
		os.Exit(0)
	}
}

func fetch() {
	cfg := getConfig()
	archiveDir = cfg.ArchiveLocation
	if !strings.HasSuffix(archiveDir, "/") {
		archiveDir = archiveDir + "/"
	}

	if len(cfg.Manga) == 0 {
		fmt.Println("You haven't added any manga to your config file yet!")
		fmt.Println("It can be found at: " + configDir + "config.json")
		os.Exit(0)
	}

	for _, manga := range cfg.Manga {
		dets, err := fetchMangaDetails(manga)
		if err != nil {
			fmt.Println("error!")
			fmt.Println(err.Error())
		}

		for chapterID := range dets.Chapters {
			if dets.Chapters[chapterID].LangCode != "gb" {
				continue
			}

			chapterSaveErrors := saveChapter(dets.Manga.Title, strconv.Itoa(chapterID))
			if len(chapterSaveErrors) > 0 {
				for _, err := range chapterSaveErrors {
					fmt.Println(err.Error())
				}
			}
		}
	}

}
