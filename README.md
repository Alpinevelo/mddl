A tool to easily download your favourite series from mangadex, because you shouldn't trust SAAS platforms to keep hosting media you care about forever.

Currently: Can dl an entire gb-language-coded translation of any manga whose ID you place in the config file.

Todo:

- download in chapter release order instead of randomly, make this optional if it turns out to slow the program down too much.
- support only downloading specified chapters
- support syncing with partial DLs to automatically fetch new uploads
- per-manga option to choose preferred TL group in case of multiple TLs per chapter
- per-manga option to choose which language to dl
- probably more

Directory structure is non-customisable and likely won't be short of moving DL location, it follows a `Manga Title/ Volume Number/ Chapter Number/ Page Images` structure, in uploads that don't use volume numbers it'll be `...Title/0/Chapter Num...`.

### Config file location:

#### Linux:

`~/.config/mddl/`

#### Windows:

`Somewhere under %APPDATA%, probably`

#### MacOS:

`No idea`

### Config format

Will inevitably change as more config options are added, generated empty for you when the program is run for the first time

- manga IDs should be inserted as strings, not numbers.
- the archiveLocation should end in a /.

  e.g,

`{ "archiveLocation": "/home/cho/myMangaCollection/", "manga": ["30461", ...] }`
