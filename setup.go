package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"os"
	"strings"
)

func setup() {
	dir, err := os.UserConfigDir()
	if err != nil {
		fmt.Println("Failed to get user config directory")
		panic(err.Error())
	}
	configDir = dir + "/mddl/"

	if fileInfo, err := os.Stat(configDir); os.IsNotExist(err) || !fileInfo.IsDir() {
		err := os.Mkdir(configDir, 0700)
		if err != nil {
			fmt.Println("Failed to create config directory")
			panic(err.Error())
		}
	}

	if _, err := os.Stat(configDir + "config.json"); os.IsNotExist(err) {
		firstTimeSetup()
	}
}

func firstTimeSetup() {
	newConfig := config{Manga: []string{"30461"}, ArchiveLocation: getArchiveLocation()}
	cfgJSON, _ := json.Marshal(newConfig)

	configFile, err := os.Create(configDir + "config.json")
	if err != nil {
		fmt.Println("Failed to create config file")
		panic(err.Error())
	}

	configFile.Write(cfgJSON)
	configFile.Close()
}

func getArchiveLocation() string {
	fmt.Println("Please enter the folder you'd like your manga to be saved in. (Default: " + configDir + "manga/" + ")")

	reader := bufio.NewReader(os.Stdin)
	archiveLoc := configDir + "manga/"

	for {
		loc, _ := reader.ReadString('\n')

		if len(strings.TrimSpace(loc)) == 0 {
			loc = archiveLoc
		}

		if !strings.HasSuffix(loc, "/") {
			loc = loc + "/"
		}

		loc = strings.ReplaceAll(loc, "\n", "")

		if _, err := os.Stat(loc); os.IsNotExist(err) {
			err := os.MkdirAll(loc, 0700)
			if err != nil {
				fmt.Println("Failed to create archive directory.")
				fmt.Println(err.Error())
				continue
			}
		}

		archiveLoc = loc
		break
	}

	return archiveLoc
}

func printMenu() {
	fmt.Println("Usage:")
	fmt.Println("mddl -- prints this menu.")
	fmt.Println("mddl setup -- runs or re-runs the first time setup process.")
	fmt.Println("mddl fetch -- downloads manga in config file to configured archive location.")
}
