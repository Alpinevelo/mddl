package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"time"
)

type apiResponse struct {
	Manga apiMangaResponse `json:"manga"`

	// int representing the mangadex chapter ID
	Chapters map[int]apiChapterResponse `json:"chapter"`
}

type apiMangaResponse struct {
	CoverURL string `json:"cover_url"`
	Title    string `json:"title"`
	OrigLang string `json:"lang_flag"`
}

type apiChapterResponse struct {
	ChapterNo string `json:"chapter"`
	VolumeNo  string `json:"volume"`
	Title     string `json:"title"`
	LangCode  string `json:"lang_code"`
	TLGroup   string `json:"group_name"`
}

type apiIndividualChapterResponse struct {
	ID        int      `json:"id"`
	Hash      string   `json:"hash"`
	Volume    string   `json:"volume"`
	ChapterNo string   `json:"chapter"`
	ServerURL string   `json:"server"`
	Pages     []string `json:"page_array"`
	Title     string   `json:"title"`
}

func fetchMangaDetails(mangaID string) (apiResponse, error) {
	res, err := http.Get("https://mangadex.org/api/manga/" + mangaID)
	if err != nil {
		return apiResponse{}, err
	}
	defer res.Body.Close()

	var body apiResponse

	err = json.NewDecoder(res.Body).Decode(&body)
	if err != nil {
		return apiResponse{}, err
	}

	return body, nil
}

func saveChapter(mangaTitle, chapterID string) []error {
	chapterRes, err := http.Get("https://mangadex.org/api/chapter/" + chapterID)
	if err != nil {
		return []error{err}
	}
	if chapterRes.StatusCode == 404 {
		fmt.Println("https://mangadex.org/api/chapter/" + chapterID)
		return []error{errors.New("Chapter " + chapterID + "not found")}
	}
	defer chapterRes.Body.Close()

	var chapter apiIndividualChapterResponse

	err = json.NewDecoder(chapterRes.Body).Decode(&chapter)
	if err != nil {
		fmt.Println(chapterRes.Status)
		fmt.Println("Error decoding: " + mangaTitle + ", chapter " + chapterID)
		fmt.Printf("verbose: %#v\n", err)
		return []error{err}
	}

	// Lazy fix for mangadex having no default volume, easier than multiple checks for one
	if chapter.Volume == "" {
		chapter.Volume = "0"
	}

	if err := os.MkdirAll(archiveDir+mangaTitle+"/"+chapter.Volume+"/"+chapter.ChapterNo+"/", 0700); err != nil {
		return []error{err}
	}

	downloadErrors := []error{}

	fmt.Println("Downloading " + mangaTitle + " Volume " + chapter.Volume + " Chapter " + chapter.ChapterNo)
	for _, page := range chapter.Pages {

		out, err := os.Create(archiveDir + mangaTitle + "/" + chapter.Volume + "/" + chapter.ChapterNo + "/" + page)
		if err != nil {
			return []error{err}
		}

		pageRes, err := http.Get(chapter.ServerURL + chapter.Hash + "/" + page)
		if err != nil {
			downloadErrors = append(downloadErrors, err)
			fmt.Println("Error downloading: " + mangaTitle + " Volume " + chapter.Volume + " Chapter " + chapter.ChapterNo + ", Page" + page)
			time.Sleep(500 * time.Millisecond)
			continue
		}

		_, err = io.Copy(out, pageRes.Body)
		out.Close()
		pageRes.Body.Close()

		fmt.Println("Downloaded " + page)

		time.Sleep(500 * time.Millisecond)
	}

	return []error{}
}
