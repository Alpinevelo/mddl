package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
)

type config struct {
	Manga           []string `json:"manga"`
	ArchiveLocation string   `json:"archiveLocation"`
}

func getConfig() config {
	file, err := os.Open(configDir + "config.json")
	if err != nil {
		fmt.Println("Couldn't read config file.")
		panic(err.Error())
	}
	defer file.Close()

	dat, _ := ioutil.ReadAll(file)

	var cfg config
	json.Unmarshal(dat, &cfg)

	return cfg
}
